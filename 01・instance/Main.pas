unit Main;

interface //#################################################################### ��

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  vulkan;

type
  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    { private �錾 }
  public
    { public �錾 }
  end;

var
  Form1: TForm1;

implementation //############################################################### ��

{$R *.fmx}

procedure TForm1.FormCreate(Sender: TObject);
const
     APP_SHORT_NAME :PAnsiChar = 'Instance';
var
   app_info  :T_VkApplicationInfo;
   inst_info :T_VkInstanceCreateInfo;
   inst      :T_VkInstance;
   res       :T_VkResult;
begin
     (* VULKAN_KEY_START *)

     // initialize the VkApplicationInfo structure
     app_info.sType              := VK_STRUCTURE_TYPE_APPLICATION_INFO;
     app_info.pNext              := nil;
     app_info.pApplicationName   := APP_SHORT_NAME;
     app_info.applicationVersion := 1;
     app_info.pEngineName        := APP_SHORT_NAME;
     app_info.engineVersion      := 1;
     app_info.apiVersion         := VK_API_VERSION;

     // initialize the VkInstanceCreateInfo structure
     inst_info.sType                   := VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
     inst_info.pNext                   := nil;
     inst_info.flags                   := 0;
     inst_info.pApplicationInfo        := @app_info;
     inst_info.enabledExtensionCount   := 0;
     inst_info.ppEnabledExtensionNames := nil;
     inst_info.enabledLayerCount       := 0;
     inst_info.ppEnabledLayerNames     := nil;

     res := vkCreateInstance( @inst_info, nil, @inst );
     if res = VK_ERROR_INCOMPATIBLE_DRIVER then
     begin
          ShowMessage( 'cannot find a compatible Vulkan ICD' );
          Exit;
     end
     else
     if res <> T_VkResult.VK_SUCCESS then
     begin
          ShowMessage( 'unknown error' );
          Exit;
     end;

     vkDestroyInstance( inst, nil );

     (* VULKAN_KEY_END *)
end;

end. //######################################################################### ��
