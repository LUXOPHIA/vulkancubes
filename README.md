﻿# VulkanCubes
Vulkan で箱を描画するまでの道程。

## プロジェクトリスト
1. instance
1. device
1. descriptor_pipeline_layouts
1. initshaders
1. uniformbuffer
1. allocdescriptorsets
1. initcommandbuffer
1. initswapchain
1. depthbuffer
1. inittexture
1. initrenderpass
1. initframebuffers
1. vertexbuffer
1. initpipeline
1. drawcube　`※ 第１ゴール`
1. drawtexturedcube　`※ 第２ゴール`

## 必須関数リスト
![VulkanCubes Functions](https://docs.google.com/uc?id=1DWNL5-uBCEojntfusUmiCbdIGEVRvogKhg&export=view "VulkanCubes Functions")
