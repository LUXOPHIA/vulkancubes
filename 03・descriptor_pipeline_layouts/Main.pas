unit Main;

interface //#################################################################### ��

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  vulkan,
  LUX.Code.C,
  device.Core,
  descriptor_pipeline_layouts.Core;

type
  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    { private �錾 }
  public
    { public �錾 }
  end;

var
  Form1: TForm1;

implementation //############################################################### ��

{$R *.fmx}

procedure TForm1.FormCreate(Sender: TObject);
var
   res                       :T_VkResult;
   info                      :T_sample_info;
   sample_title              :P_char;
   layout_binding            :T_VkDescriptorSetLayoutBinding;
   descriptor_layout         :T_VkDescriptorSetLayoutCreateInfo;
   pPipelineLayoutCreateInfo :T_VkPipelineLayoutCreateInfo;
   i                         :T_int;
begin
     sample_title := 'Descriptor / Pipeline Layout Sample';

     init_global_layer_properties( info );
     init_instance( info, sample_title );

     init_enumerate_device( info);
     init_queue_family_index( info );
     init_device( info );

     (* VULKAN_KEY_START *)
     (* Start with just our uniform buffer that has our transformation matrices
      * (for the vertex shader). The fragment shader we intend to use needs no
      * external resources, so nothing else is necessary
      *)

     (* Note that when we start using textures, this is where our sampler will
      * need to be specified
      *)
     layout_binding.binding            := 0;
     layout_binding.descriptorType     := VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
     layout_binding.descriptorCount    := 1;
     layout_binding.stageFlags         := T_VkShaderStageFlags( VK_SHADER_STAGE_VERTEX_BIT );
     layout_binding.pImmutableSamplers := nil;

     (* Next take layout bindings and use them to create a descriptor set layout
      *)
     descriptor_layout.sType        := VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
     descriptor_layout.pNext        := nil;
     descriptor_layout.bindingCount := 1;
     descriptor_layout.pBindings    := @layout_binding;

     SetLength( info.desc_layout, NUM_DESCRIPTOR_SETS );
     res := vkCreateDescriptorSetLayout( info.device, @descriptor_layout, nil, @info.desc_layout[0] );
     Assert( res = VK_SUCCESS );

     (* Now use the descriptor layout to create a pipeline layout *)
     pPipelineLayoutCreateInfo.sType                  := VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
     pPipelineLayoutCreateInfo.pNext                  := nil;
     pPipelineLayoutCreateInfo.pushConstantRangeCount := 0;
     pPipelineLayoutCreateInfo.pPushConstantRanges    := nil;
     pPipelineLayoutCreateInfo.setLayoutCount         := NUM_DESCRIPTOR_SETS;
     pPipelineLayoutCreateInfo.pSetLayouts            := @info.desc_layout[0];

     res := vkCreatePipelineLayout( info.device, @pPipelineLayoutCreateInfo, nil, @info.pipeline_layout );
     Assert( res = VK_SUCCESS );
     (* VULKAN_KEY_END *)

     for i := 0 to NUM_DESCRIPTOR_SETS - 1 do vkDestroyDescriptorSetLayout( info.device, info.desc_layout[i], nil );
     vkDestroyPipelineLayout( info.device, info.pipeline_layout, nil );
     destroy_device( info );
     destroy_instance( info );
end;

end. //######################################################################### ��
