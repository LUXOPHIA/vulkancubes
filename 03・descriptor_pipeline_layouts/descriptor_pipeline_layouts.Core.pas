unit descriptor_pipeline_layouts.Core;

interface //#################################################################### ��

uses vulkan,
     LUX.Code.C,
     device.Core;

const //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$�y�萔�z

      (* Number of descriptor sets needs to be the same at alloc,       *)
      (* pipeline layout creation, and descriptor set layout creation   *)
      NUM_DESCRIPTOR_SETS = 1;

//type //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$�y�^�z

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$�y���[�`���z

function init_global_layer_properties( var info:T_sample_info ) :T_VkResult;

procedure init_queue_family_index( var info:T_sample_info );

function init_device( var info:T_sample_info ) :T_VkResult;

procedure destroy_device( var info:T_sample_info );

implementation //############################################################### ��

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$�y�^�z

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$�y���[�`���z

function init_global_extension_properties( var layer_props:T_layer_properties ) :T_VkResult;
var
   instance_extensions      :P_VkExtensionProperties;
   instance_extension_count :T_uint32_t;
   res                      :T_VkResult;
   layer_name               :P_char;
begin
     layer_name := layer_props.properties.layerName;

     repeat
           res := vkEnumerateInstanceExtensionProperties( layer_name, @instance_extension_count, nil );
           if res <> VK_SUCCESS then
           begin
                Result := res;  Exit;
           end;

           if instance_extension_count = 0 then
           begin
                Result := VK_SUCCESS;
           end;

           SetLength( layer_props.extensions, instance_extension_count );
           instance_extensions := @layer_props.extensions[0];
           res := vkEnumerateInstanceExtensionProperties( layer_name, @instance_extension_count, instance_extensions );

     until res <> VK_INCOMPLETE;

     Result := res;
end;

function init_global_layer_properties( var info:T_sample_info ) :T_VkResult;
var
   instance_layer_count :T_uint32_t;
   vk_props             :array of T_VkLayerProperties;
   res                  :T_VkResult;
   i                    :T_uint32_t;
   layer_props          :T_layer_properties;
begin
     vk_props := nil;

     (*
      * It's possible, though very rare, that the number of
      * instance layers could change. For example, installing something
      * could include new layers that the loader would pick up
      * between the initial query for the count and the
      * request for VkLayerProperties. The loader indicates that
      * by returning a VK_INCOMPLETE status and will update the
      * the count parameter.
      * The count parameter will be updated with the number of
      * entries loaded into the data pointer - in case the number
      * of layers went down or is smaller than the size given.
      *)
     repeat
           res := vkEnumerateInstanceLayerProperties( @instance_layer_count, nil );
           if res <> VK_SUCCESS then
           begin
                Result := res;  Exit;
           end;

           if instance_layer_count = 0 then
           begin
                Result := VK_SUCCESS;  Exit;
           end;

           SetLength( vk_props, instance_layer_count );

           res := vkEnumerateInstanceLayerProperties( @instance_layer_count, @vk_props[0] );

     until res <> VK_INCOMPLETE;

     (*
      * Now gather the extension list for each instance layer.
      *)
     for i := 0 to instance_layer_count - 1 do
     begin
          layer_props.properties := vk_props[i];
          res := init_global_extension_properties( layer_props );
          if res <> VK_SUCCESS then
          begin
               Result := res;  Exit;
          end;
          info.instance_layer_properties := info.instance_layer_properties + [ layer_props ];
     end;

     Result := res;
end;

procedure init_queue_family_index( var info:T_sample_info );
var
   found :T_bool;
   i     :T_unsigned_int;
begin
     (* This routine simply finds a graphics queue for a later vkCreateDevice,
      * without consideration for which queue family can present an image.
      * Do not use this if your intent is to present later in your sample,
      * instead use the init_connection, init_window, init_swapchain_extension,
      * init_device call sequence to get a graphics and present compatible queue
      * family
      *)

     vkGetPhysicalDeviceQueueFamilyProperties( info.gpus[0], @info.queue_count, nil );
     Assert( info.queue_count >= 1 );

     SetLength( info.queue_props, info.queue_count );
     vkGetPhysicalDeviceQueueFamilyProperties( info.gpus[0], @info.queue_count, @info.queue_props[0] );
     Assert( info.queue_count >= 1 );

     found := false;
     for i := 0 to info.queue_count - 1 do
     begin
          if info.queue_props[i].queueFlags and T_VkQueueFlags( VK_QUEUE_GRAPHICS_BIT ) <> 0 then
          begin
               info.graphics_queue_family_index := i;
               found := true;
               break;
          end;
     end;
     Assert( found );
end;

function init_device( var info:T_sample_info ) :T_VkResult;
var
   res              :T_VkResult;
   queue_info       :T_VkDeviceQueueCreateInfo;
   queue_priorities :array [ 0..0 ] of T_float;
   device_info      :T_VkDeviceCreateInfo;
begin
     queue_priorities[0] := 0;
     queue_info.sType            := VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
     queue_info.pNext            := nil;
     queue_info.queueCount       := 1;
     queue_info.pQueuePriorities := @queue_priorities[0];
     queue_info.queueFamilyIndex := info.graphics_queue_family_index;

     device_info.sType                   := VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
     device_info.pNext                   := nil;
     device_info.queueCreateInfoCount    := 1;
     device_info.pQueueCreateInfos       := @queue_info;
     device_info.enabledLayerCount       := Length( info.device_layer_names );
     if device_info.enabledLayerCount <> 0 then
     device_info.ppEnabledLayerNames     := @info.device_layer_names[0]
                                           else
     device_info.ppEnabledLayerNames     := nil;
     device_info.enabledExtensionCount   := Length( info.device_extension_names );
     if device_info.enabledExtensionCount <> 0 then
     device_info.ppEnabledExtensionNames := @info.device_extension_names[0]
                                               else
     device_info.ppEnabledExtensionNames := nil;
     device_info.pEnabledFeatures        := nil;

    res := vkCreateDevice( info.gpus[0], @device_info, nil, @info.device );
    assert( res = VK_SUCCESS );

    Result := res;
end;

procedure destroy_device( var info:T_sample_info );
begin
     vkDestroyDevice( info.device, nil );
end;

end. //######################################################################### ��
