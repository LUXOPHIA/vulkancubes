﻿program descriptor_pipeline_layouts;

uses
  System.StartUpCopy,
  FMX.Forms,
  Main in 'Main.pas' {Form1},
  LUX.Code.C in '..\_LIBRARY\LUXOPHIA\LUX.Code\LUX.Code.C.pas',
  vulkan in '..\_LIBRARY\LUXOPHIA\LUX.GPGPU.Vulkan\Vulkan\vulkan.pas',
  LUX in '..\_LIBRARY\LUXOPHIA\LUX\LUX.pas',
  device.Core in '..\02・device\device.Core.pas',
  descriptor_pipeline_layouts.Core in 'descriptor_pipeline_layouts.Core.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
