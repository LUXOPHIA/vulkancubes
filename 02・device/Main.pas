unit Main;

interface //#################################################################### ��

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  vulkan,
  LUX.Code.C,
  device.Core;

type
  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    { private �錾 }
  public
    { public �錾 }
  end;

var
  Form1: TForm1;

implementation //############################################################### ��

{$R *.fmx}

procedure TForm1.FormCreate(Sender: TObject);
var
   info             :T_sample_info;
   queue_info       :T_VkDeviceQueueCreateInfo;
   found            :T_bool;
   i                :T_unsigned_int;
   queue_priorities :array [ 0..0 ] of T_float;
   device_info      :T_VkDeviceCreateInfo;
   device           :T_VkDevice;
   res              :T_VkResult;
begin
     init_instance( info, 'vulkansamples_device' );

     init_enumerate_device( info );

     (* VULKAN_KEY_START *)

     vkGetPhysicalDeviceQueueFamilyProperties( info.gpus[0], @info.queue_count, nil );
     Assert( info.queue_count >= 1 );

     SetLength( info.queue_props, info.queue_count );
     vkGetPhysicalDeviceQueueFamilyProperties( info.gpus[0], @info.queue_count, @info.queue_props[0] );
     Assert( info.queue_count >= 1 );

     found := false;
     for i := 0 to info.queue_count - 1 do
     begin
          if ( info.queue_props[i].queueFlags and T_VkQueueFlags( VK_QUEUE_GRAPHICS_BIT ) ) <> 0 then
          begin
               queue_info.queueFamilyIndex := i;
               found := true;
               Break;
          end;
     end;
     Assert( found );
     Assert( info.queue_count >= 1 );

     queue_priorities[0]         := 0;
     queue_info.sType            := VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
     queue_info.pNext            := nil;
     queue_info.queueCount       := 1;
     queue_info.pQueuePriorities := @queue_priorities[0];

     device_info.sType                   := VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
     device_info.pNext                   := nil;
     device_info.queueCreateInfoCount    := 1;
     device_info.pQueueCreateInfos       := @queue_info;
     device_info.enabledExtensionCount   := 0;
     device_info.ppEnabledExtensionNames := nil;
     device_info.enabledLayerCount       := 0;
     device_info.ppEnabledLayerNames     := nil;
     device_info.pEnabledFeatures        := nil;

     res := vkCreateDevice( info.gpus[0], @device_info, nil, @device );
     Assert( res = VK_SUCCESS );

     vkDestroyDevice( device, nil );

     (* VULKAN_KEY_END *)

     destroy_instance( info );
end;

end. //######################################################################### ��
