﻿program device;

uses
  System.StartUpCopy,
  FMX.Forms,
  Main in 'Main.pas' {Form1},
  device.Core in 'device.Core.pas',
  LUX.Code.C in '..\_LIBRARY\LUXOPHIA\LUX.Code\LUX.Code.C.pas',
  vulkan in '..\_LIBRARY\LUXOPHIA\LUX.GPGPU.Vulkan\Vulkan\vulkan.pas',
  LUX in '..\_LIBRARY\LUXOPHIA\LUX\LUX.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
