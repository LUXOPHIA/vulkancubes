unit device.Core;

interface //#################################################################### ��

uses Winapi.Windows,
     vulkan,
     LUX.Code.C;

const //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$�y�萔�z

      APP_NAME_STR_LEN = 80;

type //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$�y�^�z

     //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% T_texture_object
     (*
      * structure to track all objects related to a texture.
      *)

     T_texture_object = record
       sampler     :T_VkSampler;
       image       :T_VkImage;
       imageLayout :T_VkImageLayout;
       mem         :T_VkDeviceMemory;
       view        :T_VkImageView;
       tex_width   :T_int32_t;
       tex_height  :T_int32_t;
     end;

     //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% T_swap_chain_buffer
     (*
      * Keep each of our swap chain buffers' image, command buffer and view in one
      * spot
      *)

     T_swap_chain_buffer = record
       image :T_VkImage;
       view  :T_VkImageView;
     end;

     //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% T_layer_properties
     (*
      * A layer can expose extensions, keep track of those
      * extensions here.
      *)

     T_layer_properties = record
       properties :T_VkLayerProperties;
       extensions :array of T_VkExtensionProperties;
     end;

     //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% T_sample_info

     P_sample_info = ^T_sample_info;
     T_sample_info = record
       connection :T_HINSTANCE;                               // hInstance - Windows Instance
       name       :array [ 0..APP_NAME_STR_LEN-1 ] of T_char; // Name to put on the window/icon
       window     :T_HWND;                                    // hWnd - window handle

       surface            :T_VkSurfaceKHR;
       prepared           :T_bool;
       use_staging_buffer :T_bool;

       instance_layer_names          :array of P_char;
       instance_extension_names      :array of P_char;
       instance_layer_properties     :array of T_layer_properties;
       instance_extension_properties :array of T_VkExtensionProperties;
       inst                          :T_VkInstance;

       device_layer_names          :array of P_char;
       device_extension_names      :array of P_char;
       device_layer_properties     :array of T_layer_properties;
       device_extension_properties :array of T_VkExtensionProperties;
       gpus                        :array of T_VkPhysicalDevice;
       device                      :T_VkDevice;
       queue                       :T_VkQueue;
       graphics_queue_family_index :T_uint32_t;
       gpu_props                   :T_VkPhysicalDeviceProperties;
       queue_props                 :array of T_VkQueueFamilyProperties;
       memory_properties           :T_VkPhysicalDeviceMemoryProperties;

       framebuffers  :P_VkFramebuffer;
       width, height :T_int;
       format        :T_VkFormat;

       swapchainImageCount :T_uint32_t;
       swap_chain :T_VkSwapchainKHR;
       buffers :array of T_swap_chain_buffer;
       presentCompleteSemaphore :T_VkSemaphore;

       cmd_pool :T_VkCommandPool;

       depth :record
                format :T_VkFormat;
                image  :T_VkImage;
                mem    :T_VkDeviceMemory;
                view   :T_VkImageView;
              end;

       textures :array of T_texture_object;

       uniform_data :record
                       buf         :T_VkBuffer;
                       mem         :T_VkDeviceMemory;
                       buffer_info :T_VkDescriptorBufferInfo;
                     end;

       texture_data  :record
                        image_info :T_VkDescriptorImageInfo;
                      end;
       stagingMemory :T_VkDeviceMemory;
       stagingImage  :T_VkImage;

       vertex_buffer :record
                        buf         :T_VkBuffer;
                        mem         :T_VkDeviceMemory;
                        buffer_info :T_VkDescriptorBufferInfo;
                      end;
       vi_binding    :T_VkVertexInputBindingDescription;
       vi_attribs    :array [ 0..1 ] of T_VkVertexInputAttributeDescription;

       //glm::mat4 Projection;
       //glm::mat4 View;
       //glm::mat4 Model;
       //glm::mat4 MVP;

       cmd             :T_VkCommandBuffer; // Buffer for initialization commands
       pipeline_layout :T_VkPipelineLayout;
       desc_layout     :array of T_VkDescriptorSetLayout;
       pipelineCache   :T_VkPipelineCache;
       render_pass     :T_VkRenderPass;
       pipeline        :T_VkPipeline;

       shaderStages :array [ 0..1 ] of T_VkPipelineShaderStageCreateInfo;

       desc_pool :T_VkDescriptorPool;
       desc_set :array of T_VkDescriptorSet;

       dbgCreateDebugReportCallback  :PFN_vkCreateDebugReportCallbackEXT;
       dbgDestroyDebugReportCallback :PFN_vkDestroyDebugReportCallbackEXT;
       dbgBreakCallback              :PFN_vkDebugReportMessageEXT;
       debug_report_callbacks        :array of T_VkDebugReportCallbackEXT;

       current_buffer :T_uint32_t;
       queue_count    :T_uint32_t;

       viewport :T_VkViewport;
       scissor  :T_VkRect2D;
     end;

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$�y���[�`���z

function init_instance( var info:T_sample_info; const app_short_name:P_char ) :T_VkResult;

function init_enumerate_device( var info:T_sample_info; gpu_count:T_uint32_t = 1 ) :T_VkResult;

procedure destroy_instance( var info:T_sample_info );

implementation //############################################################### ��

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$�y�^�z

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$�y���[�`���z

function init_instance( var info:T_sample_info; const app_short_name:P_char ) :T_VkResult;
var
   app_info  :T_VkApplicationInfo;
   inst_info :T_VkInstanceCreateInfo;
   res       :T_VkResult;
begin
     app_info.sType              := VK_STRUCTURE_TYPE_APPLICATION_INFO;
     app_info.pNext              := nil;
     app_info.pApplicationName   := app_short_name;
     app_info.applicationVersion := 1;
     app_info.pEngineName        := app_short_name;
     app_info.engineVersion      := 1;
     app_info.apiVersion         := VK_API_VERSION;

     inst_info.sType                   := VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
     inst_info.pNext                   := nil;
     inst_info.flags                   := 0;
     inst_info.pApplicationInfo        := @app_info;
     inst_info.enabledLayerCount       := Length( info.instance_layer_names );
     if Length( info.instance_layer_names ) > 0 then
     inst_info.ppEnabledLayerNames     := @info.instance_layer_names[0]
     else
     inst_info.ppEnabledLayerNames     := nil;
     inst_info.enabledExtensionCount   := Length( info.instance_extension_names );
     inst_info.ppEnabledExtensionNames := @info.instance_extension_names[0];

     res := vkCreateInstance( @inst_info, nil, @info.inst );
     Assert( res = VK_SUCCESS );

     Result := res;
end;

function init_enumerate_device( var info:T_sample_info; gpu_count:T_uint32_t = 1 ) :T_VkResult;
var
   req_count :T_uint32_t;
   res       :T_VkResult;
begin
     req_count := gpu_count;
     res := vkEnumeratePhysicalDevices( info.inst, @gpu_count, nil );
     Assert( gpu_count > 0 );
     SetLength( info.gpus, gpu_count );

     res := vkEnumeratePhysicalDevices( info.inst, @gpu_count, @info.gpus[0] );
     assert( ( res = VK_SUCCESS ) and ( gpu_count >= req_count ) );

     vkGetPhysicalDeviceQueueFamilyProperties( info.gpus[0], @info.queue_count, nil );
     Assert( info.queue_count >= 1 );

     SetLength( info.queue_props, info.queue_count );
     vkGetPhysicalDeviceQueueFamilyProperties( info.gpus[0], @info.queue_count, @info.queue_props[0] );
     Assert( info.queue_count >= 1 );

     (* This is as good a place as any to do this *)
     vkGetPhysicalDeviceMemoryProperties( info.gpus[0], @info.memory_properties );
     vkGetPhysicalDeviceProperties( info.gpus[0], @info.gpu_props );

     Result := res;
end;

procedure destroy_instance( var info:T_sample_info );
begin
     vkDestroyInstance( info.inst, nil );
end;

end. //######################################################################### ��
